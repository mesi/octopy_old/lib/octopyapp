import paho.mqtt.client as mqtt
import logging
import sys
import json
from time import time
import traceback
from os import path
from time import sleep
from threading import Lock
from lib.config_loader import config_loader
from os import path
sys.path.insert(0, path.abspath(path.dirname(__file__)))
from topics import get_topics


MQTT_QOS = 1


class ConfigurationError(Exception):
    pass


class OctopyApp:
    def __init__(self, app_id, log_level = logging.INFO, config = None):
        self.connected = False
        self.mqtt_client = None
        self.mqtt_mutex = Lock()
        self.log_level = log_level
        self._subscriptions = {}
        # Load config
        self.config = {}
        try:
            self.config = config_loader.load()
        except Exception as err:
            if not isinstance(config, dict):
                self.log(logging.ERROR, f"Could not load config and no command line arguments given: {err}")
                self.halt("No configuration")
        if isinstance(config, dict):
            self.config = config_loader.merge_configs(config, self.config)
        self.verify_config()
        self.topics = get_topics(self.config['mqtt']['prefix'])
        self.app_id = app_id
        self.setup_logging()
        self.log(logging.INFO, f"Starting {self.app_id}...")


    def verify_config(self):
        # Check MQTT settings and set to default values (localhost:1883) if missing
        if not ('mqtt' in self.config):
            self.config['mqtt'] = {}
        if not ('host' in self.config['mqtt']) or not self.config['mqtt']['host'] or self.config['mqtt']['host'] == '':
            self.config['mqtt']['host'] = 'localhost'
        if not ('port' in self.config['mqtt']) or not self.config['mqtt']['port'] or self.config['mqtt']['port'] == 0:
            self.config['mqtt']['port'] = 1883


    def start(self, start_loop = True):
        self.mqtt_connect()
        if start_loop:
            self.mqtt_client.loop_forever()


    def stop(self):
        self.mqtt_client.on_disconnect = None
        self.mqtt_client.disconnect()
        self.mqtt_client.loop_stop(force = True)


    def setup_logging(self):
        # Terminal logging
        sh = logging.StreamHandler(stream = sys.stdout)
        formatter = logging.Formatter('%(levelname)s:%(message)s')
        sh.setFormatter(formatter)
        sh.setLevel(self.log_level)
        root_logger = logging.getLogger()
        root_logger.propagate = True
        root_logger.setLevel(self.log_level)
        root_logger.addHandler(sh)
        # Add callback log handler to log to MQTT
        callback_logger = CallbackLogHandler(self.log_callback)
        callback_logger.setLevel(self.log_level)
        root_logger.addHandler(callback_logger)
        self._logger = root_logger


    def mqtt_connect(self):
        # Setup MQTT connection
        client_id = f"{self.config['system id']}:{self.app_id}"
        self.mqtt_client = mqtt.Client(client_id = client_id)
        self.mqtt_client.on_connect = self.on_connect
        self.mqtt_client.on_message = self.on_message
        self.mqtt_client.on_disconnect = self.on_disconnect
        self.log(logging.INFO, f"Connecting to MQTT server '{self.config['mqtt']['host']}:{self.config['mqtt']['port']}' using client id '{client_id}'")
        if ('username' in self.config['mqtt'] and 'password' in self.config['mqtt']):
            self.mqtt_client.username_pw_set(self.config['mqtt']['username'], password=self.config['mqtt']['password'])
        self.mqtt_client.connect(self.config['mqtt']['host'], self.config['mqtt']['port'])


    def on_connect(self, client, userdata, flags, rc):
        self.log(logging.INFO, "Connected to MQTT server")
        self.connected = True
        for topic, callback in self._subscriptions.items():
            if callback:
                self.mqtt_client.message_callback_add(topic, callback)
            self.mqtt_client.subscribe(topic)


    def on_disconnect(self, client, userdata, rc):
        self.connected = False
        self.log(logging.ERROR, f"Disconnected from MQTT server. Error code: {rc}.")
        self.log(logging.INFO, "Reconnecting to MQTT server")
        sleep(1)
        self.mqtt_client.reconnect()


    def on_message(self, client, userdata, msg):
        pass


    def subscribe(self, topic, callback = None):
        self.log(logging.DEBUG, f"Subscribing to '{topic}'")
        if callback:
            self._subscriptions[topic] = callback
        else:
            self._subscriptions[topic] = None
        if self.connected and callback:
            self.mqtt_client.message_callback_add(topic, callback)
        if self.connected:
            self.mqtt_client.subscribe(topic)


    def unsubscribe(self, topic):
        self.mqtt_client.unsubscribe(topic)
        self.mqtt_client.message_callback_remove(topic)


    def publish(self, topic, payload, retain = False):
        self.mqtt_mutex.acquire()
        self.mqtt_client.publish(topic = topic, payload = payload, retain = retain, qos = MQTT_QOS)
        self.mqtt_mutex.release()


    def log(self, level, message):
        if (level == "critical" or level == logging.CRITICAL):
            self._logger.critical(message)
        elif (level == "error" or level == logging.ERROR):
            self._logger.error(message)
        elif (level == "warning" or level == logging.WARNING):
            self._logger.warning(message)
        elif (level == "info" or level == logging.INFO):
            self._logger.info(message)
        elif (level == "debug" or level == logging.DEBUG):
            self._logger.debug(message)
        else:
            raise(f"Unknown log level: {level}")


    def log_callback(self, message):
        message['source'] = self.app_id
        payload = json.dumps(message, indent = 4)
        log_topic = path.join(self.topics['logging']['prefix'], message['level'])
        if self.connected:
            self.publish(log_topic, payload)



class CallbackLogHandler(logging.Handler):
    def __init__(self, callback):
        logging.Handler.__init__(self)
        self._callback = callback

    def emit(self, record):
        message = {
            "level": record.levelname.lower(),
            "message": record.message,
            "process": record.process,
            "timestamp": time(),
            "stack": []
        }
        stack = traceback.extract_stack()[:-7]  # Hack - the last 7 calls are for the the actual logging call and we don't want to include them.
        for frame in stack:
            message['stack'].append( {
                "filename": frame.filename,
                "lineno": frame.lineno,
                "function": frame.name
            } )
        try:
            self._callback(message)
        except BaseException as e:
            print(f"Error logging message: {e}")
