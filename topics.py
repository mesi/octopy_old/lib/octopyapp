TOPICS = {
    "datasource": {
        "config serial": "$PREFIX/octopy/datasource/serial/config",
        "config socket": "$PREFIX/octopy/datasource/socket/config",
        "config mqtt": "$PREFIX/octopy/datasource/mqtt/config",
        "remove source": "$PREFIX/octopy/datasource/sources/remove",
        "sources": "$PREFIX/octopy/datasource/sources",
        "channels": "$PREFIX/octopy/channels",
        "set channels": "$PREFIX/octopy/datasource/channels/set",
        "remove channels": "$PREFIX/octopy/datasource/channels/remove",
        "update": "$PREFIX/octopy/datasource/channels/update",

        "status": "$PREFIX/octopy/datasource/hwgw/status",
        "links": "$PREFIX/octopy/datasource/hwgw/links"
    },

    "epics datasource": {
        "load links": "$PREFIX/octopy/datasource/epics/load_links"
    },

    "logging": {
        "prefix": "$PREFIX/octopy/log/"
    },

    "node manager": {
        "structure": "$PREFIX/octopy/structure",
        "update": "$PREFIX/octopy/structure/update",
	"set node": "$PREFIX/octopy/nodemanager/node/set",
        "delete node": "$PREFIX/octopy/nodemanager/node/delete",
        "load node": "$PREFIX/octopy/nodemanager/node/load"
    },

    "sequencer": {
        "load sequences": "$PREFIX/sequencer/sequences",
        "add sequence": "$PREFIX/sequencer/sequences/add"
    },

    "storage": {
        "set": "$PREFIX/octopy/storage/set",
        "get": "$PREFIX/octopy/storage/get",
        "mget": "$PREFIX/octopy/storage/mget",
        "delete": "$PREFIX/octopy/storage/delete",
        "default return": "$PREFIX/octopy/storage/return"
    },

    "recorder": {
        "query": "$PREFIX/octopy/recorder/query",
        "default return": "$PREFIX/octopy/recorder/response"
    }
}

def get_topics(prefix, topics = TOPICS):
    new_topics = {}
    for key, value in topics.items():
        if isinstance(value, str):
            new_topics[key] = value.replace('$PREFIX', prefix)
        elif isinstance(value, dict):
            new_topics[key] = get_topics(prefix, value)
    return new_topics
